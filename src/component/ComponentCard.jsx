import React, { useState } from "react";

export default function ComponentCard({ DataDetail,setData}) {
  const [ShowDetail , setTrip] = useState({});
// SET Data
 const Title=(e)=>{
  const newTitle = e.target.value
  setTrip({...ShowDetail,title:newTitle})
 }
 // Set Description
 const Description=(e)=>{
  const newDescription = e.target.value
  setTrip({...ShowDetail,description:newDescription})
}
// set PeopleGoing
const PeopleGoing=(e)=>{
  const newPeopleGoing = e.target.value
  setTrip({...ShowDetail,peopleGoing:newPeopleGoing})
}

//set Status
const Status=(e)=>{
  const newStatus = e.target.value
  setTrip({...ShowDetail,status:newStatus})
}
// Submit Button
const handleSubmit=()=>{
setData([...DataDetail,{...ShowDetail,id:DataDetail.length + 1 }])

}

// Button Read Detail
  const handleButton = (id) => {
    setTrip(id);
  };

  // Handle Button Status
  const handleChangeButton = (id) =>{
    console.log(id)
  const changStatus = DataDetail.map((Item) => {
    if(Item.id===id){
      return {
        ...Item,status:(Item.status=
          Item.status=="beach"
        ? "mountain"
        : Item.status=="mountain"
        ? "forest"
        : "beach")
      }
    }
    return Item;
  }
  )
  setTrip(changStatus);
  }

  return (
    <div>
      <div class=" mt-3 flex">
        <h1 className=" text-left text-3xl text-gray-500 font-bold">
          Good Everning Team!
        </h1>

      {/* Button Add New Trip*/}
        <label htmlFor="my-modal" className="btn ml-[600px] rounded-lg p-3 px-7  btn-success">
        ADD NEW TRIP
        </label>
        {/* Form InputData */}
        <input type="checkbox" id="my-modal" className="modal-toggle" />
        <div className="modal">
          <div className="modal-box relative bg-white">
          <label htmlFor="my-modal" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
          <h3 className="text-lg font-bold text-left my-2 text-black">Title</h3>
            <input
            onChange={Title}
              type="text"
              placeholder="Sihaknou Ville"
              className="input input-bordered border-blue-700 w-full bg-white text-black"
            />

            <h3 className="text-lg font-bold text-left my-2 text-black">Description</h3>
            <input
              onChange={Description}
              type="text"
              placeholder="Happy place with beautiful beach"
              className="input input-bordered border-blue-700  w-full bg-white text-black"
            />

            <h3 className="text-lg font-bold text-left my-2 text-black">People going</h3>
            <input 
            onChange={PeopleGoing}
              type="text"
              placeholder="3200"
              className="input input-bordered border-blue-700 w-full bg-white text-black"
            />

            <h3 className="text-lg font-bold text-left my-2  text-black">Type of adventure</h3>
            <select onChange={Status} className="select select-bordered w-full border-sky-700 bg-white text-black">
              <option disabled selected >
               ---- Choose any option ----
              </option>
              <option>Beach</option>
              <option>Mountain</option>
              <option>forest</option>
            </select>

            <div className="modal-action">
              <label onClick={handleSubmit} htmlFor="my-modal" className=" btn">
               Submit
              </label>
            </div>
          </div>
        </div>
      </div>

      {/* Card */}

      <div className=" grid  grid-cols-3 justify-evenly mt-10">
        {DataDetail.map((Item) => (
          <div class="card w-80 bg-slate-600 text-white text-left mt-10">
            <div class=" p-5 items-center">
              <h2 class="card-title uppercase">{Item.title}</h2>
              <br />
              <p className="line-clamp-3 ">{Item.description}</p>
              <br />
              <div>
                <p>People Coding</p>
                <p className=" font-bold text-xl">{Item.peopleGoing}</p>
                <br />
              </div>

              <div class="card-actions justify-end">

                <button onClick={() => handleChangeButton(Item.id)} class={`btn ${Item.status==="mountain" 
                  ? 'btn-' 
                  : (Item.status==="forest") 
                  ? 'btn-success' : 'btn-info'}`}> {Item.status}</button>

                <label
                  onClick={() => handleButton(Item)}
                  htmlFor={`my-modal-${Item.id}`}
                  className="btn btn-success"
                >
                  READ DETAIL
                </label>
                <input
                  type="checkbox"
                  id={`my-modal-${Item.id}`}
                  className="modal-toggle"
                />
                <div className="modal">
                  <div className="modal-box relative bg-lime-900">
                    <label
                      htmlFor={`my-modal-${Item.id}`}
                      className="btn btn-sm btn-circle absolute right-2 top-2"
                    >
                      ✕
                    </label>
                    <h3 className="text-lg font-bold">{ShowDetail.title}</h3>
                    <p className="py-4">{ShowDetail.description}</p>
                    <p className="py-4">
                      Around {ShowDetail.peopleGoing} people going here
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
