import React from "react";

import category_icon from '../Images/category_icon.png'
import cube from '../Images/cube.png'
import List from '../Images/list.png'
import messenger from '../Images/messenger.png'
import success from '../Images/success.png'
import security from '../Images/security.png'
import users from '../Images/users.png'
import lachlan from '../Images/lachlan.jpg'
import raamin from '../Images/raamin.jpg'
import nonamesontheway from '../Images/nonamesontheway.jpg'
import plus from '../Images/plus.png'


export default function ComponentMenu() {
  return (
    <div>
    <ul class="menu h-screen bg-gray-300 w-28">
       {/* Category */}
      <li>
        <a >
          <img className="  w-6 h-6 m-auto" src={category_icon} alt=""/>
        </a>
      </li>
         {/* Cube */}
      <li>
        <a  className=" mt-5">
          <img className=" w-6 h-6 m-auto" src={cube} alt=""/>
        </a>
      </li>
         {/* List */}
      <li>
        <a >
          <img className=" w-6 h-6 m-auto relative" src={List} alt=""/>
          <span className=" absolute top-2 right-9  badge badge-xs badge-success indicator-item "></span>
        </a>
      </li>
        {/* Message */}
      <li>
        <a >
          <img className="w-6 h-6 m-auto relative" src={messenger} alt=""/>
          <span className=" absolute top-2 right-9  badge badge-xs badge-success indicator-item "></span>
        </a>
      </li>
        {/* List */}
      <li>
        <a >
          <img className=" w-6 h-6 m-auto "  src={List} alt=""/>
        </a>
      </li>
       {/* success */}
      <li>
        <a  className=" mt-5">
          <img className=" w-6 h-6 m-auto" src={success} alt=""/>
        </a>
      </li>
       {/* security */}
      <li>
        <a >
          <img className=" w-6 h-6 m-auto" src={security} alt=""/>
        </a>
      </li>
      {/* users */}
      <li>
        <a >
          <img className=" w-6 h-6 m-auto"  src={users} alt=""/>
        </a>
      </li>

{/* Picture under option above*/}
  <div className=" mt-7">
    <div class="stat-figure text-secondary">
      <div class="avatar ">
        <div class="w-10 rounded-full">
        <a>
          <img src={lachlan} />
        </a>
        </div>
      </div>
    </div>

    <div class="stat-figure text-secondary mt-4">
      <div class="avatar ">
        <div class="w-10 rounded-full">
        <a>
          <img src={raamin} />
        </a>
        </div>
      </div>
    </div>

    <div class="stat-figure text-secondary mt-4">
      <div class="avatar ">
        <div class="w-10 rounded-full">
          <img src={nonamesontheway} />
  
        </div>
      </div>
    </div>
          <img className="w-10 h-10 mt-4 m-auto" src={plus} alt=""/>
</div>
    </ul>
    </div>
  );
}
