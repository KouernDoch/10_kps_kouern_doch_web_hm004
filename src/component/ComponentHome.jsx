import React from 'react'
import notification from '../Images/notification.png'
import comment from '../Images/comment.png'
import lachlan from '../Images/lachlan.jpg'
import raamin from '../Images/raamin.jpg'
import nonamesontheway from '../Images/nonamesontheway.jpg'

export default function ComponentHome() {
  return (
    <div>
    <div className=' h-screen bg-no-repeat bg-cover bg-[url("https://png.pngtree.com/thumb_back/fh260/background/20220217/pngtree-cartoon-hand-drawn-night-camping-illustration-background-image_953309.jpg")]'>
    <ul class=" absolute right-5 top-10 flex m-9">
      <li>
          <img className=" w-8 mx-3" src={notification} alt=""/>
      </li>
      <li>
          <img className=" w-8 mx-3" src={comment} alt=""/>
      </li>
      <div class="stat-figure text-secondary mx-3">
      <div class="avatar ">
        <div class="w-9 rounded-full">
          <img src={lachlan} />
        </div>
      </div>
    </div>
      </ul>
      {/* Button My amazing trip */}
      <div class="absolute mt-40 right-10">
      <button class=" rounded-lg p-3 px-7 btn-success ">My amazing trip</button>
    </div>
    {/* Title */}
    <div className='absolute top-64 text-slate-50 text-4xl text-left px-5'>
    <p>I like laying down on the sand and looking at the moon.</p>
    </div>

    <div className='absolute  top-[55%] text-slate-50 text-xl text-left px-5'>
    <p>27 people going to this trip</p>
    </div>

{/* Picture people */}
    <div className="absolute avatar-group top-[60%] px-5 gap-6">
  <div className="avatar border-white">
    <div className="w-12">
      <img src={lachlan} />
    </div>
  </div>
  <div className="avatar">
    <div className="w-12">
      <img src={raamin} />
    </div>
  </div>
  <div className="avatar border-blue-600">
    <div className="w-12">
      <img src={nonamesontheway} />
    </div>
  </div>
  <div className="avatar placeholder bg-white border-red-400">
    <div className="w-12 bg-neutral-focus text-neutral-content">
      <span>+23</span>
    </div>
  </div>
</div>

   </div>
    </div>
  )
}
